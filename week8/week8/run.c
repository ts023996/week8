#include <stdio.h> //Import stdio.h

float calculateInterest(float currentMoney, float interestRate);//Declared the calculateInterest function
float calculateInterestRate(float currentMoney);//declares the calculateInterestRate function

int main() {

	float initialDeposit;//to store the intial deposit in a float
	char sentinel;//to store the sentinel
	int maxMonth;//to store the number of months they want to output a block for
	float currentMoney;//to store the current money value
	int pause;//to pause the program

	printf("Please input your initial Deposit: \x9C");//asking for input of initial deposit
	scanf("%f", &initialDeposit);
	
	while (1) {//infinite loop
		currentMoney = initialDeposit;//resets the currentMoney value
		printf("\nDo you wish to see a block of months balances? (y/n)"); // asks if they wish to continue
		scanf(" %c", &sentinel);

		if (sentinel == 'n') break; // breaks if sentinel is entered

		printf("\nHow many months do you wish to see the balance for?");//asks for input of number of months
		scanf("%d", &maxMonth);

		for (int i = 1; i <= maxMonth; i++) { // loops outputting the balance for the number of money in the block of months
			printf("\nThe balance for month %d is \x9C%f", i, currentMoney);
			currentMoney = calculateInterest(currentMoney, calculateInterestRate(currentMoney)); // calculates the new value for currentMoney
			if (currentMoney < -1000) { //checks if the overdraft has been reached
				printf("\nWARNING... OVERDRAFT MAX WILL BE EXCEEDED");
				break;
			}
		}
	}

	scanf("%d", &pause); // pauses the program
	return 0;
}

/*
*This function calculates the new value of money after applying interest
*/
float calculateInterest(float currentMoney, float interestRate) {
	float newMoney = currentMoney * interestRate;
	return newMoney;
}

/*
*This function calculates the new interest value after the current money has changed.
*/
float calculateInterestRate(float currentMoney) {
	float interestRate;

	if (currentMoney < -500) {
		interestRate = 1.8;
	}
	else if (currentMoney < -100) {
		interestRate = 1.4;
	}
	else if (currentMoney < 0) {
		interestRate = 1.2;
	}
	else if (currentMoney < 100) {
		interestRate = 1.1;
	}
	else if (currentMoney < 200) {
		interestRate = 1.5;
	}
	else if (currentMoney < 500) {
		interestRate = 1.8;
	}
	else {
		interestRate = 1.9;
	}

	return interestRate;
}